

.PHONY: default
default: all

.PHONY: all
all: nexa caddy api web

.PHONY: deploy
all: deploy-web deploy-api deploy-caddy deploy-nexa deploy-compose

.PHONY: nexa
nexa: .setup-qemu
	make -C nexa clean
	docker build -t homeautomation-nexabuild nexa
	docker run -t --rm -v `pwd`/nexa:/nexa --workdir /nexa -e CROSS_TRIPLE=arm-linux-gnueabi homeautomation-nexabuild make

.PHONY: caddy
caddy: .setup-qemu
	docker build -t homeautomation-caddy caddy

.PHONY: api
api: .setup-qemu .node-image
	docker build -t homeautomation-api api

.PHONY: web
web: .setup-qemu .node-image
	docker build -t homeautomation-web web

.PHONY: deploy-nexa
deploy-nexa: nexa
	.make-scripts/push-nexa

.PHONY: deploy-caddy
deploy-caddy: caddy
	.make-scripts/push-image homeautomation-caddy

.PHONY: deploy-api
deploy-api: api
	.make-scripts/push-image homeautomation-api

.PHONY: deploy-web
deploy-web: web
	.make-scripts/push-image "homeautomation-web"

.PHONY: deploy-compose
deploy-compose: compose
	.make-scripts/push-compose

.PHONY: .setup-qemu
.setup-qemu:
	# Install binfmt_misc binaries in host
	[ ! -f /usr/bin/qemu-arm-static ] && docker run --rm --privileged multiarch/qemu-user-static:register --reset

.PHONY: .node-image
.node-image:
	docker build -t homeautomation-node node-image
