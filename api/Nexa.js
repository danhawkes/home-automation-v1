const execFile = require('child_process').execFile;

class BadRequestError extends Error {
  constructor(message) {
    super(message);
    this.name = 'BadRequestError';
  }
}

function sendPacket(data) {
  if (data == undefined) {
    return Promise.reject(new BadRequestError('"data" is missing.'));
  } else if (data.length != 32) {
    return Promise.reject(
      new BadRequestError(
        `"data" length must be 32 bits; received ${data.length}.`
      )
    );
  } else if (!/^[10]*$/.test(data)) {
    return Promise.reject(
      new BadRequestError(
        `"data" contains invalid characters: "${data.replace(/[10]/g, '.')}".`
      )
    );
  }
  return new Promise((resolve, reject) => {
    if (process.env.NODE_ENV === 'production') {
      execFile('nexa', ['tx', data], (err, stdout, stderr) => {
        if (err) {
          reject(err);
        } else {
          resolve(stdout);
        }
      });
    } else {
      setTimeout(() => {
        let r = Math.random();
        if (r > 0.5) {
          resolve();
        } else {
          reject(new Error('Simulated error'));
        }
      }, 500);
    }
  });
}

module.exports = {
  BadRequestError,
  sendPacket
};
