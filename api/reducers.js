const SERIAL_1 = '10101111011101111100010101';
const SERIAL_2 = '10101111011101111100010111';

const initialState = {
  channel: '11',
  group: '1',
  switches: {
    living_tv: {
      id: 'living_tv',
      serial: SERIAL_1,
      unit: '11',
      name: 'Living room tv area',
      on: false
    },
    living_table: {
      id: 'living_table',
      serial: SERIAL_1,
      unit: '10',
      name: 'Living room table area',
      on: false
    },
    stairs: {
      id: 'stairs',
      serial: SERIAL_2,
      unit: '01',
      name: 'Stairs',
      on: false
    },
    bedroom: {
      id: 'bedroom',
      serial: SERIAL_1,
      unit: '01',
      name: 'Bedroom',
      on: false
    }
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SWITCH':
      return Object.assign({}, state, {
        switches: Object.assign({}, state.switches, {
          [action.id]: Object.assign({}, state.switches[action.id], {
            on: action.on
          })
        })
      });
    default:
      return state;
  }
};

module.exports = reducer;
