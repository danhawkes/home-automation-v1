#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nexa.yucc"
#include "rx.h"
#include "tx.h"

void error(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
}

int main(int argc, char **argv) {
  int rc = 0;
  yuck_t argp[1];
  yuck_parse(argp, argc, argv);

  switch (argp->cmd) {
    case NEXA_CMD_TX: {
      if (argp->nargs == 0) {
        error("Data parameter is required\n");
        rc = 1;
        goto exit;
      } else if (argp->nargs > 1) {
        error("A single data parameter is required\n");
        rc = 1;
        goto exit;
      }
      char *datastr = *argp->args;
      int datastr_len = strlen(datastr);
      if (datastr_len != 32) {
        error("Data length must be 32 bits, received %d\n", datastr_len);
        rc = 1;
        goto exit;
      }
      printf("Sending command…\n");
      uint8_t data[4] = {0};
      for (int i = 0; i < 32; i++) {
        uint8_t val = datastr[i] == '1';
        int byte = i / 8;
        int bit = 7 - (i % 8);
        data[byte] |= (val << bit);
      }

      setupTx();
      wirePacket(data, 32);

      break;
    }
    case NEXA_CMD_RX: {
      setupRx();
      uint8_t data[4] = {0};

      if (!readPacket(data)) {
      } else {
        error("Failed to read packet");
        rc = 1;
        goto exit;
      }
      break;
    }
    default:
      yuck_auto_usage(argp);
  }

exit:
  yuck_free(argp);
  return rc;
}
