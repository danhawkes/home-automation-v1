#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>
#include <wiringPi.h>

#include "rx.h"

static const int PIN = 6;

void setupRx() {
  wiringPiSetup();
  pinMode(PIN, OUTPUT);
}

int readPacket(uint8_t *data) {
  int size = 4194304;
  int *values = calloc(size, sizeof(int));
  unsigned int *timestamps = calloc(size, sizeof(unsigned int));

  for (int i = 0; i < size; i++) {
    int value = digitalRead(PIN);
    unsigned int timestamp = micros();
    values[i] = value;
    timestamps[i] = timestamp;
  }

  for (int i = 0; i < size; i++) {
    printf("%d %u\n", values[i], timestamps[i]);
  }

  free(values);
  free(timestamps);

  return 0;
}
