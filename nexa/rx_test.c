#include "rx.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct item {
  int value;
  unsigned int timestamp;
} item;

item *loadItems(int size) {
  FILE *f = fopen("3", "r");
  if (!f) {
    return NULL;
  }

  item *items = calloc(size, sizeof(item));

  int value = 0;
  unsigned int timestamp = 0;

  for (int i = 0; i < size; i++) {
    if (fscanf(f, "%d %u", &value, &timestamp) != EOF) {
      items[i].value = value;
      items[i].timestamp = timestamp;
    }
  }
  return items;
}

bool roughly(int expected, int actual) {
  int error_margin = expected / 10;
  return (actual > (expected - error_margin)) &&
         (actual < (expected + error_margin));
}

const int T = 250;
item previousItem;
item currentItem;

typedef void (*funcptr)();
typedef funcptr (*StateFunc)();

StateFunc idle();
StateFunc start();
StateFunc data();

StateFunc idle() {
  if (currentItem.value == 1) {
    printf("s ");
    return (StateFunc)start;
  }
  return (StateFunc)idle;
}

unsigned int startHigh = 0;
unsigned int startLow = 0;
bool detectingHigh = true;
StateFunc start() {
  // If this is the first call to start(), then item[-1] has the time of the
  // first bit.
  if (startHigh == 0) {
    startHigh = previousItem.timestamp;
  }
  if (detectingHigh) {
    if (currentItem.value == 0) {
      unsigned int pulseDuration = (currentItem.timestamp - startHigh);
      // High period has ended, is duration long enough?
      if (roughly(T, pulseDuration)) {
        startLow = currentItem.timestamp;
        detectingHigh = false;
      } else {
        printf("i ");
        startHigh = 0;
        startLow = 0;
        detectingHigh = true;
        return (StateFunc)idle;
      }
    }
  } else {
    if (currentItem.value == 1) {
      unsigned int pulseDuration = (currentItem.timestamp - startLow);
      // Low period has ended, is duration long enough?
      if (roughly(T * 10, pulseDuration)) {
        printf("data ");
        startHigh = 0;
        startLow = 0;
        detectingHigh = true;
        return (StateFunc)data;
      } else {
        printf("i ");
        startHigh = 0;
        startLow = 0;
        detectingHigh = true;
        return (StateFunc)idle;
      }
    }
  }
  return (StateFunc)start;
}

unsigned int data1High;
unsigned int data1Low;
unsigned int data2High;
unsigned int data2Low;
int databitMode;
StateFunc data() { return (StateFunc)idle; }

int main() {
  int size = 4194304;
  item *items = loadItems(size);

  printf("i ");
  StateFunc stateFunc = (StateFunc)idle;

  for (int i = 0; i < size; i++) {
    previousItem = currentItem;
    currentItem = items[i];
    stateFunc = (StateFunc)stateFunc();

    // printf("%d %u\n", items[i].value, items[i].timestamp);
  }
  free(items);

  return 0;
}