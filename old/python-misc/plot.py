#!/usr/bin/env python3

import csv
import matplotlib.pyplot as pyplot
# import numpy as np
# import scipy.signal as signal

pyplot.rcParams['agg.path.chunksize'] = 50000

value = []
time = []
with open('../../web/tx/1') as datafile:
    csv_reader = csv.reader(datafile, delimiter=' ')
    prev_t = 0
    prev_v = -1
    for row in csv_reader:
        v = int(row[0])
        t = float(row[1])
        if (t - prev_t) >= 125:
            if (v != prev_v):
                prev_v = v
                value.append(v)
                time.append(t)
            prev_t = t

print("values = {}".format(len(value)))

skip_plot = False
if not skip_plot:
    min = 0
    count = 10000
    max = min + count
    time = time[min:max]
    value = value[min:max]

    fig = pyplot.figure()
    # pyplot.axis([0, 700000, 0.0, 1.0])
    # pyplot.grid()
    # pyplot.fill(time, value, 'b')
    pyplot.fill_between(time, value, color='b')
    pyplot.show()
