const PROD = process.env.NODE_ENV === 'production';
const PORT = process.env.PORT || 3000;

const express = require('express');
const app = express();

if (!PROD) {
  throw new Error('Server not used in dev builds: use react-scripts');
}

app.use(express.static('./build'));
app.listen(PORT);

console.log(
  `Started web server [mode: ${PROD ? 'prod' : 'dev'}, port: ${PORT}]`
);
