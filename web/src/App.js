import React, { Component } from 'react';
import './App.css';

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, error: false };
    fetch('/api/switches', {
      credentials: 'include'
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          throw new Error();
        }
      })
      .then(json => {
        this.setState({ loading: false, error: false, switches: json });
      })
      .catch(err => {
        this.setState({ error: true });
      });
  }

  sendCommand(id, on) {
    return fetch(`/api/switch?id=${id}&on=${on}`, {
      method: 'POST',
      credentials: 'include'
    });
  }

  setSwitchOn(id, on) {
    let newState = {
      ...this.state,
      switches: {
        ...this.state.switches,
        [id]: { ...this.state.switches[id], on: on }
      }
    };
    this.setState(newState);
  }

  render() {
    let content;
    if (this.state.error) {
      content = this.renderError();
    } else if (this.state.loading) {
      content = this.renderLoading();
    } else {
      content = this.renderApp();
    }
    return content;
  }

  renderLoading() {
    return (
      <div className="App-loading-container">
        <h1 className="App-loading-text">loading…</h1>
      </div>
    );
  }
  renderError() {
    return (
      <div className="App-error-container">
        <h1 className="App-error-text">an error occurred</h1>
        <button
          className="App-button"
          onClick={() => {
            window.location.reload();
            this.setState({ loading: true, error: false });
          }}>
          Retry
        </button>
      </div>
    );
  }
  renderApp() {
    return (
      <div className="App">
        <div className="App-togglebutton-container">
          {Object.values(this.state.switches).map((s, i) => (
            <div key={i}>{this.renderButton(s.id, s.name, s.on)}</div>
          ))}
        </div>
      </div>
    );
  }
  renderButton(id, name, on) {
    return (
      <button
        className={
          'App-button App-togglebutton ' +
          (on ? 'App-togglebutton-on' : 'App-togglebutton-off')
        }
        onClick={() => {
          this.setSwitchOn(id, !on);
          this.sendCommand(id, !on)
            .then(resp => {
              if (!resp.ok) {
                throw new Error('Bad response code');
              }
            })
            .catch(e => this.setSwitchOn(id, on));
        }}>
        {name}
      </button>
    );
  }
}
